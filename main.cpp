#include <iostream>
#include <pthread.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <ctime>
#include <unistd.h>
#include <algorithm>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstdlib>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <string>

using namespace std;
const string NAZOV_SUBORU_SERVER = "svet_server";
const string NAZOV_SUBORU_KLIENT = "svet_klient";
const string CESTA_K_SUBOROM = "/home/vydra6/semestralka/";

int pauzaVypisy; /// pocet milisekund medzi vypismi do konzoly

typedef struct struct_mravec {
    int x; /// index na osi x na hracom poli
    int y; /// index na osi y na hracom poli
    int smer; /// 1-hore, 2-vpravo, 3-dole, 4-vľavo
    int id; /// identifikátor mravca
    bool jeMrtvy; /// true-mravec zomrel, false-mravec žije
} MRAVEC;

typedef struct struct_policko {
    bool ciernaFarba; /// false-biela, true-čierna
    bool jeMravec; /// false-mravec nie je na políčku, true-mravec je na políčku
} POLICKO;

typedef struct struct_hracie_pole {
    const int velkostX; /// šírka poľa
    const int velkostY; /// výška poľa
    POLICKO **pole; /// hracie pole políčok
    vector<int> *poradie; /// obsahuje list s poradím mravcov
    pthread_mutex_t *mutex; /// synchronizácia
    pthread_cond_t *pohyb; /// synchronizácia
} HRACIE_POLE;

typedef struct struct_hracie_data {
    int pravidloPrezitia; /// 1-prežije iba jeden mravec
    int logikaMravcov; /// 1-klasická, 2-inverzná
    HRACIE_POLE *hraciePole;
    MRAVEC mravec;
} HERNE_DATA;

string zadajAdresuServera() {
    string adresa = "";
    cout << "ZADAJ ADRESU SERVERA" << endl;
    cout << "Vstup: ";
    cin >> adresa;
    return adresa;
}

int zadajPortServera() {
    int port = -1;

    while (true) {
        cout << "ZADAJ PORT SERVERA" << endl;
        cout << "Vstup: ";
        cin >> port;
        if (port <= 1024) {
            cout << "\nNesprávne číslo portu. Port musí byť väčší ako 1024." << endl;
        } else {
            return port;
        }
    }
}
string getAktualnyCas() {
    time_t casTeraz = time(0);
    tm *lokalnyCas = localtime(&casTeraz);
    string hodiny = lokalnyCas->tm_hour < 10 ? "0" + to_string(lokalnyCas->tm_hour) : to_string(lokalnyCas->tm_hour);
    string minuty = lokalnyCas->tm_min < 10 ? "0" + to_string(lokalnyCas->tm_min) : to_string(lokalnyCas->tm_min);
    string sekundy = lokalnyCas->tm_sec < 10 ? "0" + to_string(lokalnyCas->tm_sec) : to_string(lokalnyCas->tm_sec);
    string retazec = hodiny + ":" + minuty + ":" + sekundy;
    return retazec;
}

string bufferToString(char *buffer, int dlzkaBuffra) {
    string ret(buffer, dlzkaBuffra);
    return ret;
}

vector<string> precitajSubor(string nazovBezPripony, string cesta = CESTA_K_SUBOROM) {
    string riadok;
    vector<string> riadky;
    ifstream subor(cesta + nazovBezPripony + ".txt");
    if (subor.is_open()) {
        while (getline(subor, riadok)) {
            riadky.push_back(riadok);
        }
        subor.close();
    } else {
        perror("Súbor sa nenašiel");
    }
    return riadky;
}

void ulozSvetDoSuboru(vector<string> obsah, string nazovBezPripony = NAZOV_SUBORU_KLIENT, string cesta = CESTA_K_SUBOROM) {
    ofstream subor;
    subor.open(cesta + nazovBezPripony + ".txt");
    for (auto riadok : obsah) {
        subor << riadok << endl;
    }
    subor.close();
}

void ulozSvetDoSuboru(string obsah, string nazovBezPripony = NAZOV_SUBORU_KLIENT, string cesta = CESTA_K_SUBOROM) {
    ofstream subor;
    subor.open(cesta + nazovBezPripony + ".txt");
    subor << obsah << endl;
    subor.close();
}


string riadkyNaString(vector<string> riadky) {
    string retazec = "";
    for (int i = 0; i < riadky.size(); i++) {
        if (i == riadky.size() - 1)
            retazec += riadky.at(i);
        else
            retazec += riadky.at(i) + "\n";
    }
    return retazec;
}


void funkciaServera() {
    int sockfd, newsockfd;
    socklen_t cli_len;
    struct sockaddr_in serv_addr, cli_addr;
    int n;
    char buffer[256];

    int portServera = zadajPortServera();

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portServera);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("Chyba pri vytváraní socketu.");
    }

    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("Chyba pri priradení adresy.");
    }

    listen(sockfd, 5);
    cli_len = sizeof(cli_addr);
    cout << "[" << getAktualnyCas() << "][SERVER-INFO] Server bol spustený na porte " << portServera << endl;

    while (true) {
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &cli_len);
        if (newsockfd < 0) {
            perror("Chyba pri prijatí.");
        }

        bzero(buffer, 256);
        n = read(newsockfd, buffer, 255);
        if (n < 0) {
            perror("Chyba pri čítaní zo socketu.");
        }

        string retazec = bufferToString(buffer, 256);
        retazec.erase(find(retazec.begin(), retazec.end(), '\0'), retazec.end());
        int volba = stoi(retazec);

        if (volba == 1) {
            vector<string> riadky = precitajSubor(NAZOV_SUBORU_SERVER);
            retazec = riadkyNaString(riadky);

            bzero(buffer, 256);
            strcpy(buffer, retazec.c_str());
            n = write(newsockfd, buffer, strlen(buffer));
            if (n < 0) {
                perror("Chyba zápisu do socketu.");
                exit(4);
            }
            cout << "[" << getAktualnyCas() << "][SERVER-INFO] Server odoslal dáta klientovi." << endl;
        } else if (volba == 2) {
            bzero(buffer, 256);
            cout << "[" << getAktualnyCas() << "][SERVER-INFO] Server čaká na prijatie dát." << endl;
            n = read(newsockfd, buffer, 256);
            if (n < 0) {
                perror("Chyba čítania zo socketu.");
            }
            cout << "[" << getAktualnyCas() << "][SERVER-INFO] Server prijal dáta od klienta." << endl;
            retazec = bufferToString(buffer, 256);
            retazec.erase(find(retazec.begin(), retazec.end(), '\0'), retazec.end());
            ulozSvetDoSuboru(retazec, NAZOV_SUBORU_SERVER);
            cout << "[" << getAktualnyCas() << "][SERVER-INFO] Server uložil dáta do súboru." << endl;
        }
        close(newsockfd);
    }
    close(sockfd);
}


int zadajVolbu(vector<string> list) {
    int volba = -1;

    while (true) {
        int pocitadlo = 0;
        for (auto item : list) {
            if (pocitadlo == 0) {
                cout << item << endl;
            } else {
                cout << pocitadlo << ") " << item << endl;
            }
            pocitadlo++;
        }

        cout << "Voľba: ";
        cin >> volba;

        if (wcin.fail()) {
            cin.clear();
            cin.ignore();
            wcout << L"\nNesprávny výber! Skús znova." << endl;
        } else if (volba > 0 && volba < list.size()) {
            return volba;
        } else {
            cout << "Nesprávna voľba. Prosím zadajte znova." << endl;
        }
    }
}

int zadajTypAplikacie() {
    vector<string> list;
    list.emplace_back("\nTYP APLIKÁCIE NA SPUSTENIE");
    list.emplace_back("Server");
    list.emplace_back("Klient");
    list.emplace_back("Koniec");
    return zadajVolbu(list);
}

int zobrazMenu() {
    vector<string> list;
    list.emplace_back("\nHLAVNÉ MENU");
    list.emplace_back("Simulácia");
    list.emplace_back("Stiahnúť vzor zo servera");
    list.emplace_back("Koniec");
    return zadajVolbu(list);
}

int zacatSimulaciu() {
    vector<string> list;
    list.emplace_back("\nZAČAŤ SIMULÁCIU");
    list.emplace_back("Áno");
    list.emplace_back("Nie (návrat do menu)");
    list.emplace_back("Koniec");
    return zadajVolbu(list);
}

int nastaveniaSimulacie() {
    vector<string> list;
    list.emplace_back("\nNASTAVENIA SIMULÁCIE");
    list.emplace_back("Vygenerovať svet");
    list.emplace_back("Načítať svet z lokálneho súboru");
    list.emplace_back("Nastaviť manuálne");
    list.emplace_back("Návrat do menu");
    list.emplace_back("Koniec");
    return zadajVolbu(list);
}

int zadajPravidloPrezitia() {
    vector<string> list;
    list.emplace_back("\nPRAVIDLO PREŽITIA");
    list.emplace_back("Prežije iba 1 mravec");
    return zadajVolbu(list);
}

int zadajLogikuMravcov() {
    vector<string> list;
    list.emplace_back("\nLOGIKA MRAVCOV");
    list.emplace_back("Klasická");
    list.emplace_back("Inverzná");
    return zadajVolbu(list);
}

int vyzvaNaUlozenieSveta(bool lokalne) {
    vector<string> list;
    if (lokalne)
        list.emplace_back("\nPRAJETE SI ULOŽIŤ SVET DO LOKÁLNEHO SÚBORU?");
    else
        list.emplace_back("\nPRAJETE SI ULOŽIŤ SVET DO SÚBORU NA SERVER?");
    list.emplace_back("Áno");
    list.emplace_back("Nie");
    list.emplace_back("Koniec");
    return zadajVolbu(list);
}

int zadajSirkuMapy() {
    int sirka = -1;

    while (true) {
        cout << "\nZADAJ ŠÍRKU MAPY (os X)" << endl;
        cout << "Vstup: ";
        cin >> sirka;
        if (sirka < 5 || sirka > 100) {
            cout << "\nNesprávna šírka! Povolený rozsah 5-100" << endl;
        } else {
            return sirka;
        }
    }
}

int zadajVyskuMapy() {
    int vyska = -1;

    while (true) {
        cout << "\nZADAJ VÝŠKU MAPY (os Y)" << endl;
        cout << "Vstup: ";
        cin >> vyska;
        if (vyska < 5 || vyska > 100) {
            cout << "\nNesprávna výška! Povolený rozsah 5-100" << endl;
        } else {
            return vyska;
        }
    }
}

int zadajPocet(string typ, int velkostX, int velkostY) {
    int pocetMravcov = -1;

    while (true) {
        cout << "\nZADAJ POČET " << typ << endl;
        cout << "Vstup: ";
        cin >> pocetMravcov;
        if (pocetMravcov < 1 || pocetMravcov > ((velkostX * velkostY) / 2)) {
            cout << "\nNesprávny počet! Povolený rozsah 1-" << ((velkostX * velkostY) / 2) << endl;
        } else {
            return pocetMravcov;
        }
    }
}

POLICKO **vytvorHraciePole(int velkostY, int velkostX) {
    auto **pole = new POLICKO *[velkostY];
    for (int i = 0; i < velkostY; i++) {
        pole[i] = new POLICKO[velkostX];
    }

    for (int i = 0; i < velkostY; i++) {
        for (int j = 0; j < velkostX; j++) {
            pole[i][j].ciernaFarba = false;
            pole[i][j].jeMravec = false;
        }
    }

    return pole;
}

void vymazatHraciePole(POLICKO **pole, int velkostY) {
    for (int i = 0; i < velkostY; i++) {
        delete pole[i];
    }
    delete[] pole;
}

void vygenerujFarbuPolicok(HRACIE_POLE hraciePole, const int pocetCiernychPolicok) {
    int pocetNastavenych = 0;

    while (pocetNastavenych < pocetCiernychPolicok) {
        int y = rand() % hraciePole.velkostY;
        int x = rand() % hraciePole.velkostX;
        if (!hraciePole.pole[y][x].ciernaFarba) {
            hraciePole.pole[y][x].ciernaFarba = true;
            pocetNastavenych++;
        }
    }
}

MRAVEC *vygenerujMravcov(HRACIE_POLE hraciePole, const int pocetMravcov) {
    int pocetNastavenych = 0;
    MRAVEC *mravce = new MRAVEC[pocetMravcov];

    while (pocetNastavenych < pocetMravcov) {
        int y = rand() % hraciePole.velkostY;
        int x = rand() % hraciePole.velkostX;

        if (!hraciePole.pole[y][x].jeMravec) {
            hraciePole.pole[y][x].jeMravec = true;
            mravce[pocetNastavenych].id = pocetNastavenych;
            mravce[pocetNastavenych].x = x;
            mravce[pocetNastavenych].y = y;
            mravce[pocetNastavenych].jeMrtvy = false;
            mravce[pocetNastavenych].smer = (rand() % 4) + 1;
            pocetNastavenych++;
        }
    }
    return mravce;
}

int vygenerujNahodneCislo(int min, int max) {
    int rnd = (rand() % ((max - min) + 1)) + min;
    return rnd;
}

vector<string> splitString(string retazec, char znak) {
    vector<string> substringy;
    stringstream stringstr(retazec);
    string str;

    while (getline(stringstr, str, znak)) {
        substringy.push_back(str);
    }
    return substringy;
}

bool platnyRozsah(const HERNE_DATA *herneData, const int y, const int x) {
    int maxX = herneData->hraciePole->velkostX;
    int maxY = herneData->hraciePole->velkostY;

    return (y >= 0 && y < maxY && x >= 0 && x < maxX);
}

void funkciaKlienta(const int typOperacie = -1, vector<string> obsah = vector<string>()) {
    int sockfd, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[256];

    string adresaServera = zadajAdresuServera();
    int portServera = zadajPortServera();

    server = gethostbyname(adresaServera.c_str());
    if (server == nullptr) {
        perror("Chyba, neexistujúci server\n");
        return;
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(
            (char *) server->h_addr,
            (char *) &serv_addr.sin_addr.s_addr,
            server->h_length
    );
    serv_addr.sin_port = htons(portServera);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("Chyba pri vytváraní socketu");
        return;
    }

    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("Chyba pripojenia socketu.");
        return;
    }

    string retazec;
    if (typOperacie == -1) {
        cout << "1) Stiahnuť súbor" << endl;
        cout << "2) Odoslať súbor\nVoľba: " << endl;
        cin >> retazec;
    } else if (typOperacie == 1) {
        retazec = "1";
    } else if (typOperacie == 2) {
        retazec = "2";
    }

    bzero(buffer, 256);
    strcpy(buffer, retazec.c_str());
    n = write(sockfd, buffer, strlen(buffer));
    if (n < 0) {
        perror("Chyba zápisu do socketu.");
    }

    if (stoi(retazec) == 1) {
        bzero(buffer, 256);
        cout << "[" << getAktualnyCas() << "][KLIENT-INFO] Klient čaká na prijatie dát." << endl;
        n = read(sockfd, buffer, 255);
        if (n < 0) {
            perror("Chyba čítania zo socketu.");
        }
        retazec = bufferToString(buffer, 256);
        retazec.erase(find(retazec.begin(), retazec.end(), '\0'), retazec.end());
        if (!retazec.empty()) {
            ulozSvetDoSuboru(retazec, NAZOV_SUBORU_KLIENT);
            cout << "[" << getAktualnyCas() << "][KLIENT-INFO] Súbor bol úspešne stiahnutý a uložený." << endl;
        } else {
            cout << "[" << getAktualnyCas() << "][KLIENT-INFO] Súbor nebol nájdený alebo je prázdny." << endl;
        }
    } else if (stoi(retazec) == 2) {
        if (obsah.empty()) {
            vector<string> riadky = precitajSubor(NAZOV_SUBORU_KLIENT);
            retazec = riadkyNaString(riadky);
        } else {
            retazec = riadkyNaString(obsah);
        }

        bzero(buffer, 256);
        strcpy(buffer, retazec.c_str());
        usleep(500);
        n = write(sockfd, buffer, strlen(buffer));
        if (n < 0) {
            perror("Chyba zápisu do socketu.");
        }
        cout << "[" << getAktualnyCas() << "][KLIENT-INFO] Klient odoslal dáta na server." << endl;
    }
    close(sockfd);
}

int zadajPauzuMedziVypismi() {
    int pauza = -1;

    while (true) {
        cout << "\nZADAJ PAUZU MEDZI VYPISMI OD 0 DO 3000 MILISEKÚND " << endl;
        cout << "Vstup: ";
        cin >> pauza;
        if (pauza < 0 || pauza > 3000) {
            cout << "\nNesprávny počet! Povolený rozsah 0-3000 milisekúnd" << endl;
        } else {
            return pauza;
        }
    }
}

void *pohybMravca(void *arg) {
    auto herneData = (HERNE_DATA *) arg;
    int dovodSmrti = -1; /// 1-narazil na iného mravca, 2-vyšiel z mapy

    cout << "[MRAVEC " << herneData->mravec.id << " - INICIALIZÁCIA]\t\tSmer: " << herneData->mravec.smer << ", x: "
         << herneData->mravec.x << ", y: " << herneData->mravec.y << ", farba políčka: "
         << (herneData->hraciePole->pole[herneData->mravec.y][herneData->mravec.x].ciernaFarba ? "čierna" : "biela")
         << endl;
    usleep(500);

    while (!herneData->mravec.jeMrtvy) {
        pthread_mutex_lock(herneData->hraciePole->mutex);
        while (herneData->hraciePole->poradie->at(0) != herneData->mravec.id) {
//            usleep(pauzaVypisy * 1000);
//            cout << "[MRAVEC " << herneData->mravec.id << " - ČAKANIE]\t\t\tMravec začal čakať." << endl;
            pthread_cond_wait(herneData->hraciePole->pohyb, herneData->hraciePole->mutex);
        }

        int x = herneData->mravec.x;
        int y = herneData->mravec.y;

        /// zmena smeru mravca
        if ((!herneData->hraciePole->pole[y][x].ciernaFarba && herneData->logikaMravcov == 1)
            || (herneData->hraciePole->pole[y][x].ciernaFarba && herneData->logikaMravcov == 2)) {
            herneData->mravec.smer++;
            if (herneData->mravec.smer > 4) {
                herneData->mravec.smer = 1;
            }
            herneData->hraciePole->pole[y][x].ciernaFarba = true;
        } else {
            herneData->mravec.smer--;
            if (herneData->mravec.smer < 1) {
                herneData->mravec.smer = 4;
            }
            herneData->hraciePole->pole[y][x].ciernaFarba = false;
        }

        /// pohyb mravca
        if (herneData->mravec.smer == 1) {
            if (platnyRozsah(herneData, y - 1, x)) {
                if (herneData->pravidloPrezitia == 1) {
                    if (!herneData->hraciePole->pole[y - 1][x].jeMravec) {
                        herneData->mravec.y--;
                        herneData->hraciePole->pole[y - 1][x].jeMravec = true;
                    } else {
                        dovodSmrti = 1;
                        herneData->mravec.jeMrtvy = true;
                    }
                }
            } else {
                dovodSmrti = 2;
                herneData->mravec.jeMrtvy = true;
            }
        } else if (herneData->mravec.smer == 2) {
            if (platnyRozsah(herneData, y, x + 1)) {
                if (herneData->pravidloPrezitia == 1) {
                    if (!herneData->hraciePole->pole[y][x + 1].jeMravec) {
                        herneData->mravec.x++;
                        herneData->hraciePole->pole[y][x + 1].jeMravec = true;
                    } else {
                        dovodSmrti = 1;
                        herneData->mravec.jeMrtvy = true;
                    }
                }
            } else {
                dovodSmrti = 2;
                herneData->mravec.jeMrtvy = true;
            }
        } else if (herneData->mravec.smer == 3) {
            if (platnyRozsah(herneData, y + 1, x)) {
                if (herneData->pravidloPrezitia == 1) {
                    if (!herneData->hraciePole->pole[y + 1][x].jeMravec) {
                        herneData->mravec.y++;
                        herneData->hraciePole->pole[y + 1][x].jeMravec = true;
                    } else {
                        dovodSmrti = 1;
                        herneData->mravec.jeMrtvy = true;
                    }
                }
            } else {
                dovodSmrti = 2;
                herneData->mravec.jeMrtvy = true;
            }
        } else if (herneData->mravec.smer == 4) {
            if (platnyRozsah(herneData, y, x - 1)) {
                if (herneData->pravidloPrezitia == 1) {
                    if (!herneData->hraciePole->pole[y][x - 1].jeMravec) {
                        herneData->mravec.x--;
                        herneData->hraciePole->pole[y][x - 1].jeMravec = true;
                    } else {
                        dovodSmrti = 1;
                        herneData->mravec.jeMrtvy = true;
                    }
                }
            } else {
                dovodSmrti = 2;
                herneData->mravec.jeMrtvy = true;
            }
        }
        herneData->hraciePole->pole[y][x].jeMravec = false;

        if (!herneData->mravec.jeMrtvy) {
            usleep(pauzaVypisy * 1000);
            cout << "[MRAVEC " << herneData->mravec.id << " - ZMENA]\t\t\t\tNový smer: " << herneData->mravec.smer
                 << ", stará pozícia: x=" << x << ", y=" << y << ", nová pozícia: x="
                 << herneData->mravec.x << ", y=" << herneData->mravec.y << ", farba nového políčka: " <<
                 (herneData->hraciePole->pole[herneData->mravec.y][herneData->mravec.x].ciernaFarba ? "čierna"
                                                                                                    : "biela") << endl;

            int poradie = herneData->hraciePole->poradie->at(0);
            herneData->hraciePole->poradie->erase(herneData->hraciePole->poradie->begin());
            herneData->hraciePole->poradie->push_back(poradie);
        } else {
            usleep(pauzaVypisy * 1000);
            if (dovodSmrti == 1) {
                cout << "[MRAVEC " << herneData->mravec.id
                     << " - SMRŤ]\t\t\t\tMravec narazil na iného mravca a zomiera."
                     << endl;
            } else if (dovodSmrti == 2) {
                cout << "[MRAVEC " << herneData->mravec.id << " - SMRŤ]\t\t\t\tMravec vyšiel z mapy a končí aktivitu."
                     << endl;
            }

            herneData->hraciePole->poradie->erase(herneData->hraciePole->poradie->begin());
        }

        if (herneData->hraciePole->poradie->empty()) {
            cout << "---------------------[KONIEC SIMULÁCIE]---------------------" << endl;
            cout << "\nPre návrat do menu zadaj \"0\", pre ukončenie zadaj \"1\"" << endl;
        }

        pthread_mutex_unlock(herneData->hraciePole->mutex);
        pthread_cond_broadcast(herneData->hraciePole->pohyb);
    }
    return nullptr;
}

int main() {
    srand(time(nullptr));

    int velkostX;
    int velkostY;
    int pocetMravcov;
    int pocetCiernychPolicok;
    int pravidloPrezitia;
    int logikaMravcov;

    int typAplikacie = zadajTypAplikacie();
    if (typAplikacie == 1) {
        funkciaServera();
    } else if (typAplikacie == 2) {
        navratDoMenu:
        int volbaMenu = zobrazMenu();
        if (volbaMenu == 1) {
            int volbaDat = nastaveniaSimulacie();
            if (volbaDat == 1) {
                velkostX = vygenerujNahodneCislo(5, 100);
                velkostY = vygenerujNahodneCislo(5, 100);
                pocetMravcov = vygenerujNahodneCislo(1, (velkostX * velkostY) / 2);
                pocetCiernychPolicok = vygenerujNahodneCislo(1, (velkostX * velkostY) / 2);
                pravidloPrezitia = 1;
                logikaMravcov = vygenerujNahodneCislo(1, 2);
            } else if (volbaDat == 2) {
                vector<string> substringy;
                string nazovSuboru;
                cout << "\nZADAJ NÁZOV SÚBORU" << endl;
                cout << "Názov: ";
                cin >> nazovSuboru;
                vector<string> riadky = precitajSubor(nazovSuboru);

                if (!riadky.empty()) {
                    substringy = splitString(riadky.at(0), ',');
                    velkostX = stoi(substringy.at(1));

                    substringy = splitString(riadky.at(1), ',');
                    velkostY = stoi(substringy.at(1));

                    substringy = splitString(riadky.at(2), ',');
                    pocetMravcov = stoi(substringy.at(1));

                    substringy = splitString(riadky.at(3), ',');
                    pocetCiernychPolicok = stoi(substringy.at(1));

                    substringy = splitString(riadky.at(4), ',');
                    pravidloPrezitia = stoi(substringy.at(1));

                    substringy = splitString(riadky.at(5), ',');
                    logikaMravcov = stoi(substringy.at(1));
                } else {
                    goto navratDoMenu;
                }
            } else if (volbaDat == 3) {
                velkostX = zadajSirkuMapy();
                velkostY = zadajVyskuMapy();
                pocetMravcov = zadajPocet("MRAVCOV", velkostX, velkostY);
                pocetCiernychPolicok = zadajPocet("ČIERNYCH POLÍČOK", velkostX, velkostY);
                pravidloPrezitia = zadajPravidloPrezitia();
                logikaMravcov = zadajLogikuMravcov();
            } else if (volbaDat == 4) {
                goto navratDoMenu;
            } else if (volbaDat == 5) {
                return 0;
            }

            vector<string> obsah;
            obsah.push_back("velkostX," + to_string(velkostX));
            obsah.push_back("velkostY," + to_string(velkostY));
            obsah.push_back("pocetMravcov," + to_string(pocetMravcov));
            obsah.push_back("pocetCiernychPolicok," + to_string(pocetCiernychPolicok));
            obsah.push_back("pravidloPrezitia," + to_string(pravidloPrezitia));
            obsah.push_back("logikaMravcov," + to_string(logikaMravcov));

            if (volbaDat != 2) {
                int vyzvaNaUlozenieLokalne = vyzvaNaUlozenieSveta(true);
                if (vyzvaNaUlozenieLokalne == 1) {
                    ulozSvetDoSuboru(obsah);
                } else if (vyzvaNaUlozenieLokalne == 3) {
                    return 0;
                }
            }

            int vyzvaNaUlozenieServer = vyzvaNaUlozenieSveta(false);
            if (vyzvaNaUlozenieServer == 1) {
                funkciaKlienta(2, obsah);
            } else if (vyzvaNaUlozenieServer == 3) {
                return 0;
            }

            int vyzvaZacatieSimulacie = zacatSimulaciu();
            if (vyzvaZacatieSimulacie == 2) {
                goto navratDoMenu;
            } else if (vyzvaZacatieSimulacie == 3) {
                return 0;
            }

            pauzaVypisy = zadajPauzuMedziVypismi();
        } else if (volbaMenu == 2) {
            funkciaKlienta(1);
            goto navratDoMenu;
        } else if (volbaMenu == 3) {
            return 0;
        }

        cout << "\n[NASTAVENIE SIMULÁCIE]" << endl;
        cout << "\tVeľkosť X: " << velkostX << endl;
        cout << "\tVeľkosť Y: " << velkostY << endl;
        cout << "\tPočet mravcov: " << pocetMravcov << endl;
        cout << "\tPočet čiernych políčok: " << pocetCiernychPolicok << endl;
        cout << "\tPravidlo prežitia: " << (pravidloPrezitia == 1 ? "prežije 1 mravec" : "") << endl;
        cout << "\tLogika mravcov: " << (logikaMravcov == 1 ? "klasická" : "inverzná") << endl << endl;

        cout << "---------------------[ZAČIATOK SIMULÁCIE]---------------------" << endl;

        POLICKO **pole = vytvorHraciePole(velkostY, velkostX);

        pthread_t mravce_vlakna[pocetMravcov];

        pthread_mutex_t mutex;
        pthread_cond_t pohyb;
        pthread_cond_init(&pohyb, nullptr);
        pthread_mutex_init(&mutex, nullptr);

        vector<int> poradie;
        for (int i = 0; i < pocetMravcov; ++i) {
            poradie.push_back(i);
        }

        HRACIE_POLE hraciePole{
                velkostX,
                velkostY,
                pole,
                &poradie,
                &mutex,
                &pohyb,
        };
        vygenerujFarbuPolicok(hraciePole, pocetCiernychPolicok);
        MRAVEC *mravce = vygenerujMravcov(hraciePole, pocetMravcov);

        HERNE_DATA data[pocetMravcov];
        for (int i = 0; i < pocetMravcov; i++) {
            data[i].pravidloPrezitia = pravidloPrezitia;
            data[i].hraciePole = &hraciePole;
            data[i].mravec = mravce[i];
            data[i].logikaMravcov = logikaMravcov;
            pthread_create(&mravce_vlakna[i], nullptr, &pohybMravca, &data[i]);
        }

        int x = -1;
        while (x != 0) {
            cin >> x;
            if (cin && x == 0) {
                for (int i = 0; i < pocetMravcov; i++) {
                    pthread_cancel(mravce_vlakna[i]);
                }
                break;
            } else if (cin && x == 1) {
                return 0;
            } else {
                x = -1;
                cin.clear();
                cin.ignore();
            }

        }

        delete mravce;
        mravce = nullptr;
        vymazatHraciePole(pole, velkostY);
        pole = nullptr;
        pthread_cond_destroy(&pohyb);
        pthread_mutex_destroy(&mutex);
    } else if (typAplikacie == 3) {
        return 0;
    }

    goto navratDoMenu;

    return 0;
}
